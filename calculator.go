package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func mod(mod int) int { // остаток от деления
	return mod % 10
}

func div(div int) int { // целая часть от деления
	return div / 10
}

func ConvertToString(num int) string { // конвертер из int в string
	str := strconv.Itoa(num)
	return str
}

func ConvertToInt(str string) int { // конвертер из string в int
	toNumber, _ := strconv.Atoi(str)
	return toNumber
}

func RomanAnswer(a, b int) (str string) { // ответ в римских цифрах
	var str1, str2 string
	switch a {
	case 1:
		str1 = "X"
	case 2:
		str1 = "XX"
	case 3:
		str1 = "XXX"
	case 4:
		str1 = "XL"
	case 5:
		str1 = "L"
	case 6:
		str1 = "LX"
	case 7:
		str1 = "LXX"
	case 8:
		str1 = "LXXX"
	case 9:
		str1 = "XC"
	case 10:
		str1 = "C"
	}

	switch b {
	case 1:
		str2 = "I"
	case 2:
		str2 = "II"
	case 3:
		str2 = "III"
	case 4:
		str2 = "IV"
	case 5:
		str2 = "V"
	case 6:
		str2 = "VI"
	case 7:
		str2 = "VII"
	case 8:
		str2 = "VIII"
	case 9:
		str2 = "IX"
	}
	return str1 + str2
}

func RomanToArab(num string) int {
	switch num {
	case "I":
		return 1
	case "II":
		return 2
	case "III":
		return 3
	case "IV":
		return 4
	case "V":
		return 5
	case "VI":
		return 6
	case "VII":
		return 7
	case "VIII":
		return 8
	case "IX":
		return 9
	case "X":
		return 10
	}
	return 0
}

func calc(fnum int, symb string, lnum int) (result int) { // операция * / + -

	switch symb {
	case "+":
		result = fnum + lnum
	case "-":
		result = fnum - lnum
	case "*":
		result = fnum * lnum
	case "/":
		result = fnum / lnum
	}
	return result
}

func error(a, symb, b string) {

	// проверка на числа от 1 до 10

	// проверка арифмитического символа
	if symb != "+" && symb != "/" && symb != "*" && symb != "-" {
		err := errors.New("ERORR: Неверный арифметический символ")
		fmt.Println(err)
		os.Exit(1)
	}
	// проверка цифр на один тип
	if ((a == "1" || a == "2" || a == "3" || a == "4" || a == "5" || a == "6" || a == "7" || a == "8" || a == "9" || a == "10") && (b == "I" || b == "II" || b == "III" || b == "IV" || b == "V" || b == "VI" || b == "VII" || b == "VIII" || b == "IX" || b == "X")) || ((a == "I" || a == "II" || a == "III" || a == "IV" || a == "V" || a == "VI" || a == "VII" || a == "VIII" || a == "IX" || a == "X") && (b == "1" || b == "2" || b == "3" || b == "4" || b == "5" || b == "6" || b == "7" || b == "8" || b == "9" || b == "10")) {
		err := errors.New("ERORR: Нельзя производить действие с римскими и арабскими цифрами")
		fmt.Println(err)
		os.Exit(1)
	}

	// проверка на отрицательный ответ в римских цифра
	if (a == "I" || a == "II" || a == "III" || a == "IV" || a == "V" || a == "VI" || a == "VII" || a == "VIII" || a == "IX" || a == "X") && (b == "I" || b == "II" || b == "III" || b == "IV" || b == "V" || b == "VI" || b == "VII" || b == "VIII" || b == "IX" || b == "X") {
		if (calc(RomanToArab(a), symb, RomanToArab(b))) < 1 {
			err := errors.New("ERORR: В римской системе нет отрицательных чисел ")
			fmt.Println(err)
			os.Exit(1)
		}
	}

}

func check(a, symb, b string) (result string) {
	// проверка на работу с арабскими числами
	if (a == "1" || a == "2" || a == "3" || a == "4" || a == "5" || a == "6" || a == "7" || a == "8" || a == "9" || a == "10") && (b == "1" || b == "2" || b == "3" || b == "4" || b == "5" || b == "6" || b == "7" || b == "8" || b == "9" || b == "10") {
		fnum := ConvertToInt(a) // конвертер в int
		lnum := ConvertToInt(b)
		result = ConvertToString(calc(fnum, symb, lnum)) // конвертер в string
	}
	// проверка на работу с римскими  числами
	if (a == "I" || a == "II" || a == "III" || a == "IV" || a == "V" || a == "VI" || a == "VII" || a == "VIII" || a == "IX" || a == "X") && (b == "I" || b == "II" || b == "III" || b == "IV" || b == "V" || b == "VI" || b == "VII" || b == "VIII" || b == "IX" || b == "X") {
		fnum := (RomanToArab(a))           // конвертер римских в арабские
		lnum := (RomanToArab(b))           // конвертер римских в арабские
		div := div(calc(fnum, symb, lnum)) // целое число
		mod := mod(calc(fnum, symb, lnum)) // остаток
		result = RomanAnswer(div, mod)     // вывод ответа римскими цифрами
	}
	return result
}

func main() {
	var a, symb, c string // объявление переменных

	fmt.Print("Введите выражение: ")
	reader := bufio.NewReader(os.Stdin) // Разбор массива
	text, _ := reader.ReadString('\n')  //..
	text = strings.TrimSpace(text)      //..
	num := strings.Fields(text)         //..
	a = num[0]                          //..
	symb = num[1]                       //..
	c = num[2]                          //..
	if len(num) > 3 {
		err := errors.New("ERORR: Формат математической операции не удовлетворяет заданию — два операнда и один оператор (+, -, /, *) ")
		fmt.Println(err)
		os.Exit(1)
	}
	error(a, symb, c)
	fmt.Println(check(a, symb, c))
}
